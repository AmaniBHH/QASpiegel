import os
import re
import tools

script_dir = os.path.dirname(__file__) #<-- absolute dir the project is in
rel_path = "DBpediaFiles/article_categories_de.ttl"
abs_file_path = os.path.join(script_dir, rel_path)

# build a dict with multiple keys per value
article_categories = []
with open(abs_file_path, 'r') as f: #open the file
    contents = tools.loadfile(f) #put the lines to a variable.
    length = len(contents)
    for index in range(1, 20):
        line = contents[index].split()
        print(len(line))
        print(line)
        elem1 = line[0].split('resource')
        my_elem1 = re.sub("<http://de.dbpedia.org/resource/", '', elem1[len(elem1)-1])
        my_elem1 = re.sub('[_/<>]', ' ', my_elem1)
        print(my_elem1)
        elem2 = line[1].split('/')
        my_elem2 = re.sub('[<>]', '', elem2[len(elem2) - 1])

        elem3 = line[2].split('/')
        aux = re.sub('[<>]', '', elem3[len(elem3)-1])
        my_elem3 = re.sub('[_]', ' ', aux)
        # disambig_dict[disambig] = my_id
        article_categories.append([my_elem1, my_elem2, my_elem3])


print("***************************** disambiguations *************************")
print(len(article_categories))
for elem in article_categories:
    print(elem)

"""print("***************************** disambig_dict   *************************")
print(len(disambig_dict))
for elem in disambig_dict.items():
    print(elem)"""

"""print(disambig_dict.get('Marcus Pretzell'))
print(disambig_dict.get('Obama'))
print(disambig_dict.get('Adalet Partisi'))
print(disambig_dict.get('Apollo'))"""

