import os
import re
import tools

# generate the path to the file to be treated
script_dir = os.path.dirname(__file__)    # <-- absolute dir the script is in
rel_path = "DBpediaFiles/disambiguations_de.ttl"
abs_file_path = os.path.join(script_dir, rel_path)

disambiguations = []
urls = dict()
with open(abs_file_path, 'r') as f:    # open the file
    contents = tools.loadfile(f)    # put the lines to a variable.
    length = len(contents)
    L = []
    line = contents[1].split()
    L.append(line[2])
    my_url = line[0]
    for index in range(2, 40):
        next_line = contents[index].split()
        print("***************", index, "***************")
        print("line :", line[0])
        print("next line :", next_line[0])
        if next_line[0] == my_url:
            L.append(next_line[2])
        else:
            disambiguations.append([my_url, L])
            L = []
            line = contents[index].split()
            L.append(line[2])
            my_url = line[0]
        print("L :", L)

    disambiguations.append([my_url, L])

    """str1 = line[0].split('/')
    my_id = re.sub('[<>]', '', str1[len(str1)-1])
    str2 = line[2].split('/')
    aux = re.sub('[<>]', '', str2[len(str2)-1])
    disambig = re.sub('[_]', ' ', aux)
    disambiguations.append(line[:3])
    disambig_dict[disambig] = my_id
    urls[disambig] = line[2]"""


print("***************************** disambiguations *************************")
print(len(disambiguations))
for elem in disambiguations:
    print(elem)
