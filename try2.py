from SPARQLWrapper import SPARQLWrapper, JSON


def get_country_description(query):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    sparql.setQuery(query)  # the previous query as a literal string

    return sparql.query().convert()


query = "PREFIX dbres: <http://dbpedia.org/resource/>\
       SELECT ?p ?o\
  WHERE { dbres:United_States ?p ?o }"
print(get_country_description(query))
