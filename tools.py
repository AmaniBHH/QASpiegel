import spacy
nlp = spacy.load('de')


def loadfile(my_file):
    lines = []
    for line in my_file:
        lines.append(line)
    return lines


def ner_sent(sentence):
    sentence = nlp(sentence)
    ner = []
    for ent in sentence.ents:
        ner.append([ent.text, ent.label_])
    return ner
