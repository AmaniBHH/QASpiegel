import os
import re
import tools
from multi_key_dict import multi_key_dict

# generate the path to the file to be treated
script_dir = os.path.dirname(__file__)    # <-- absolute dir the script is in
rel_path = "DBpediaFiles/disambiguations_de.ttl"
abs_file_path = os.path.join(script_dir, rel_path)

disambiguations = []
# build a dict with multiple keys per value
disambig_dict = multi_key_dict()
urls = dict()
with open(abs_file_path, 'r') as f:    # open the file
    contents = tools.loadfile(f)    # put the lines to a variable.
    length = len(contents)
    for index in range(1, length):
        line = contents[index].split()
        str1 = line[0].split('/')
        my_id = re.sub('[<>]', '', str1[len(str1)-1])
        str2 = line[2].split('/')
        aux = re.sub('[<>]', '', str2[len(str2)-1])
        disambig = re.sub('[_]', ' ', aux)
        disambiguations.append(line[:3])
        disambig_dict[disambig] = my_id
        urls[disambig] = line[2]


"""print("***************************** disambiguations *************************")
print(len(disambiguations))
for elem in disambiguations:
    print(elem)

print("***************************** disambig_dict   *************************")
print(disambig_dict)
print(urls)
"""
urls_file = open('urls.csv', 'w')

for k, v in urls.items():
    # print(k, v)
    urls_file.write(k)
    urls_file.write(';')
    urls_file.write(v)
    urls_file.write('\n')



