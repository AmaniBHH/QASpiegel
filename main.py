from nltk.tokenize import sent_tokenize, word_tokenize
import tools
import csv

print("***** Beginning *****")
file = open('exp_article', 'r')
print("***** file read *****")

my_article = []
with open('exp_article', 'r') as file:  # open the file
    contents = tools.loadfile(file)  # put the lines to a variable.
    for line in contents:
        sentences = sent_tokenize(line)
        # print(type(sentences))
        for item in sentences:
            my_article.append(item)

urls = dict()
with open('urls.csv', newline='') as csvfile:
    url_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in url_reader:
        urls[row[0]] = row[1]

ner_tags = []
for elem in my_article:

    my_tag = tools.ner_tag.ner_sent(elem)
    if my_tag:
        for i in range(0, len(my_tag)):
            url = ""
            print("**************************", i, "**************************")
            print(my_tag)
            print(my_tag[i])
            sub = my_tag[i][0]
            print("sub :", sub)
            if sub in urls:
                url = urls[sub]
            print(sub, url)
            ner_tags.append([my_tag[i][0], my_tag[i][1], url])

print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
for elem in ner_tags:
    print(elem)
print("***** The end ! *****")
